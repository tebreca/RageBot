package client;

import java.io.File;
import java.util.function.BiConsumer;

public class ClientRunner {
	private static BiConsumer<File, Boolean> consumer;
	
	
	public static void run(){
		File f = new File("playerDB.xml");
		consumer.accept(f, f.exists());
	}
	
	public static void onFileSelected(BiConsumer<File, Boolean> action){
		consumer = action;
	}
}
