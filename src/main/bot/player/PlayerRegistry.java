package bot.player;

import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;

public class PlayerRegistry {

	public static List<Player> players = new ArrayList<>();

	public static Document toXML() {
		Element root = new Element("players");
		players.forEach(player -> {
			root.addContent(player.toXMLElement(0));
		});
		return new Document(root);
	}

	public static void fromXML(Document XMLDocument) {
		players.clear();
		Element allPlayers = XMLDocument.getRootElement();
		ArrayList<Element> playerElements = new ArrayList<>();
		allPlayers.getChildren().forEach(player -> {
			playerElements.add(player);
			Player p = new Player(player.getAttributeValue("id"), Integer.parseInt(player.getAttributeValue("points")));
			players.add(p);
		});

		playerElements.forEach(element -> {
			String id = element.getAttributeValue("id");
			getPlayerByID(id).fromXML(element.getChild("opponents"));
		});
	}

	public static boolean hasID(String id) {
		return players.stream().filter(player -> player.id.equals(id)).count() > 0;
	}

	public static Player getPlayerByID(String id) {
		return (Player) players.stream().filter(player -> player.id.equals(id)).toArray()[0];
	}
}
