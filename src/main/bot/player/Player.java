package bot.player;

import org.jdom2.Element;
import sx.blah.discord.api.IDiscordClient;

import java.util.HashMap;
import java.util.Map;

public class Player {
	public int points;
	public Map<Player, Boolean> opponents = new HashMap<>();
	protected String id;

	public int getPoints() {
		return points;
	}

	public String getId() {
		return id;
	}

	public Player(String id) {
		this(id, 0);
	}

	public Player(String id, int points) {
		this.id = id;
	}

	public Player(IDiscordClient client) {
		id = client.getApplicationClientID();
	}

	public Element toXMLElement(int mode) {
		if (mode == 0) {
			Element root = new Element("player").setAttribute("id", id).setAttribute("points", points + "");
			Element opponents = new Element("opponents");
			PlayerRegistry.players.forEach(player -> {
				Element opponent = player.toXMLElement(1);
				boolean flag = this.opponents.containsKey(player);
				Element battleInfo = new Element("battled");
				battleInfo.setAttribute("value", flag ? "true" : "false");
				battleInfo.setText(flag ? this.opponents.get(player) ? "true" : "false" : "");
				opponent.addContent(battleInfo);
				opponents.addContent(opponent);
			});
			root.addContent(opponents);
			return root;
		} else if (mode == 1) {
			return new Element("player").setAttribute("id", id);
		} else {
			throw new IllegalArgumentException("The mode: " + mode + "isn't supported!!");
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Player)
			return this.id.equals(((Player) obj).id);
		return false;
	}

	@Override
	public int hashCode() {
		return 2 * 1 + id.hashCode();
	}

	public void fromXML(Element XMLElement) {
		XMLElement.getChildren().forEach(opponent -> {
			boolean flag = opponent.getChild("battled").getAttributeValue("value").equals("true");
			if (flag) {
				Player player = PlayerRegistry.getPlayerByID(opponent.getAttributeValue("id"));
				if (player != null) {
					boolean fought = opponent.getChild("battled").getAttributeValue("value").equals("true");
					if (fought) {
						boolean won = opponent.getChild("battled").getText() == "true";
						this.opponents.put(player, won);
					}
				}
			}
		});
	}
}
