package bot;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import bot.events.EventHandler;
import bot.player.PlayerRegistry;
import bot.util.IOUtil;
import client.ClientRunner;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.util.DiscordException;

public class Main {

	public static void main(String[] args) {
		String key = "NDY2MTIyMTAyNTA4ODc5ODgy.DiXdlg.MXlMfqYzelzuERZy-1uszddaUZQ";
		IDiscordClient client;
		try {
			client = new ClientBuilder().withToken(key).build();
		} catch (DiscordException e) {
			e.printStackTrace();
			System.err.println("seems like something went wrong!");
			return;
		}
		client.getDispatcher().registerListener(new EventHandler());
		try {
			client.login();
		} catch (Exception e) {
			System.out.println("PC offline, entering client testing mode!!");
			/** PC is offline **/
		}
		ClientRunner.onFileSelected((File f, Boolean b) -> {
			IOUtil.setDataBase(f);
			if (b)
				initFileSystem(f);
			else
				createFileSystem(f);
		});
		ClientRunner.run();
		
	}

	private static void createFileSystem(File f) {
		XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
		try {
			outputter.output(PlayerRegistry.toXML(), new FileWriter(f));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void initFileSystem(File f) {
		SAXBuilder builder = new SAXBuilder();
		try {
			Document d = builder.build(f);
			PlayerRegistry.fromXML(d);
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
