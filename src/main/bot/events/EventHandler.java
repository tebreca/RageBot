package bot.events;

import java.util.Arrays;
import java.util.Map.Entry;

import bot.player.Player;
import bot.player.PlayerRegistry;
import bot.util.IOUtil;
import bot.util.StringUtil;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IRole;

public class EventHandler {

	static int number = 0;
	static String chache = "";

	@EventSubscriber
	public void onMessageRecievedEvent(MessageReceivedEvent event) {
		String msg = event.getMessage().getContent();
		Boolean hasRole = false;
		IRole[] roles = event.getAuthor().getRolesForGuild(event.getGuild()).toArray(new IRole[0]);
		for(IRole role : roles){
			hasRole = hasRole ? true : role.getName().equals("Tournament Manager");
		}
		if (msg.startsWith("&") && hasRole) {
			IOUtil.deserializeDataBase();
			Entry<Integer, String> command = StringUtil.tillChar(' ', msg);
			String[] augments = StringUtil.splitTill(' ', msg.substring(command.getKey()));
			String cmdID = command.getValue().substring(1).trim();
			System.out.println(cmdID);
			String id;
			IChannel c = event.getChannel();
			switch (cmdID) {
			case "test":
				c.sendMessage("TEST!!! " + Arrays.toString(augments));
				break;
			case "add":
				id = augments[0];
				if (!PlayerRegistry.hasID(id)) {
					Player p = new Player(id);
					PlayerRegistry.players.add(p);
					c.sendMessage("the player - " + id + " was succesfully added to the list of contestants!!");
				} else {
					c.sendMessage(":x: the player - " + id + " was already on the list of contestants!! :x: ");
				}
				break;

			case "remove":
				id = augments[0];
				if (!PlayerRegistry.hasID(id)) {
					c.sendMessage(":x: the player - " + id + " wasn't on the list of contestants!! :x: ");
				} else {
					PlayerRegistry.players.remove(PlayerRegistry.getPlayerByID(id));
					c.sendMessage("the player - " + id + " was succesfully removed from the list of contestants!!");
				}
				break;
			case "list":
				if (!PlayerRegistry.players.isEmpty()) {
					resetNumber();
					resetCache();
					PlayerRegistry.players.forEach(player -> {
						nextNumber();
						addToCache(number + ". - " + player.getId() + " - " + player.getPoints() + " points" + '\n');
					});
					c.sendMessage(chache);
				} else {
					c.sendMessage(":x: There are no players in the contest!! :x:");
				}
				break;

			case "fight":
				String winner = augments[0];
				String loser = augments[1];
				boolean secure1 = PlayerRegistry.hasID(winner);
				boolean secure2 = PlayerRegistry.hasID(loser);
				Player won = PlayerRegistry.getPlayerByID(winner);
				Player lost = PlayerRegistry.getPlayerByID(loser);
				boolean flag = won.opponents.containsKey(lost) || lost.opponents.containsKey(won);
				if (!flag) {
					if (secure1 && secure2) {
						if (winner != loser) {
							won.opponents.put(lost, true);
							lost.opponents.put(won, false);
							won.points++;
							c.sendMessage("Added one point to the winner: " + winner + " !!! Congrats!!");
						} else {
							c.sendMessage(":x: Sorry but you cannot win against yourselfes!! :x:");
						}
					} else {
						c.sendMessage(":x: One or more of the players aren't on the list of contestants!! :x:");
					}
				} else {
					c.sendMessage(":x: Those players fought already!! :x:");
				}
				break;

			case "getPossibleEnemies":
				id = augments[0];
				if (PlayerRegistry.hasID(id)) {
					Player p = PlayerRegistry.getPlayerByID(id);
					Object[] s = PlayerRegistry.players.stream().filter(player -> !p.opponents.containsKey(player) && p.getId() != player.getId())
							.toArray();
					if (s.length == 0) {
						c.sendMessage(":x: The player has fought everyone already!! :x:");
					} else {
						c.sendMessage(id + " Hasn't battled these players");
						resetCache();
						for (Object opponent : s) {
							if (opponent instanceof Player) {
									addToCache(" - " + ((Player) opponent).getId() + '\n');
							}
						}
						c.sendMessage(chache);
					}
				} else {
					c.sendMessage(":x: the player - " + id + " wasn't on the list of contestants!! :x:");
				}
				break;
			case "clear":
				PlayerRegistry.players.clear();
				System.gc();
				c.sendMessage("Succesfully cleared cache, contestant list has been reset!!");
			default:
				break;
			}
			IOUtil.serializeDataBase();
		}
	}

	private static void resetCache(){
		chache = "";
	}
	
	private static void addToCache(String s){
		chache+=s;
	}
	
	private static void nextNumber() {
		number++;
	}

	private static void resetNumber() {
		number = 0;
	}

}
