package bot.util;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;

public class StringUtil {

    public static Map.Entry<Integer, String> tillChar(char till, String s){
        int i = 0;
        int max = s.length();
		char current = '^';
        String cache = "";
        while(current != till && i < max){
        	current = s.charAt(i);
        	cache += current;
        	i++;
        }
        return new AbstractMap.SimpleEntry<>(i, cache.substring(0, cache.length()));
    }

    public static String[] splitTill(char till, String s){
       int i = 0, max = s.length();
       String cache = s;
       ArrayList<String> values = new ArrayList<>();
       while(i < max){
    	   Map.Entry<Integer, String> nextword = tillChar(till, cache);
    	   i+=nextword.getKey();
    	   System.out.println(nextword.getValue().trim());
    	   cache = s.substring(i);
    	   values.add(nextword.getValue().trim());
       }
       return values.toArray(new String[0]);
    }


}
