package bot.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import bot.player.PlayerRegistry;

public class IOUtil {

	private static File xmlDB;

	public static void setDataBase(File DB) {
		xmlDB = DB;
	}

	public static void serializeDataBase() {
		XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
		try {
			outputter.output(PlayerRegistry.toXML(), new FileWriter(xmlDB));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void deserializeDataBase() {
		SAXBuilder builder = new SAXBuilder();
		try {
			Document d = builder.build(xmlDB);
			PlayerRegistry.fromXML(d);
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
